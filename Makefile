OPTS=-O2
LDFLAGS=-L. -lds -lpthread

squad: builder.h builder.c parser.o libds.a
	gcc $(OPTS) parser.o builder.c $(LDFLAGS) -o squad

parser.o: parser.h parser.c
	gcc $(OPTS) -c parser.c

vector.o: vector.h vector.c
	gcc $(OPTS) -c vector.c

map.o: map.h map.c
	gcc $(OPTS) -c map.c

strutils.o: strutils.h strutils.c
	gcc $(OPTS) -c strutils.c

clean:
	rm -f squad *.o 
