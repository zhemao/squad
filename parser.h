#ifndef __SQUAD_PARSER_H__
#define __SQUAD_PARSER_H__

#include "ds.h"
#include <stdio.h>

struct task{
	char * output;
	vector * commands;
	vector * dependencies;
};

typedef struct {
	map * vars;
	map * tasks;
} squad_file;

void free_task(void*);
/* Returns 1 if line is a comment (begins with '#'), an empty string,
   or is all whitespace (spaces and tabs). Returns 0 otherwise. */
int ignorable(char * line);
/* Parses the file with name fname and puts all variable and tasks
   into vars and tasks. If successful, return 0. Otherwise, returns the
   number of the line on which the error occurred. */
int parse_squad_file(FILE * f, squad_file * sqfile);

squad_file * squad_file_new();
void squad_file_free(void *);

#endif
