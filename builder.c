#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>

#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>

#include "parser.h"
#include "builder.h"

#define THREAD_EXIT_OK 0x01ab

static squad_file * sqfile;

pthread_t start_task_thread(struct task * task){
	pthread_t tid;
	pthread_create(&tid, NULL, perform_task, (void*)task);
	return tid;
}

int needs_update(struct task * task){
	int i;
	char * fname;
	struct stat outst, depst;
	int res;
	struct task * subtask;
	
	/* if the output file does not exist, the task needs updating */
	res = stat(task->output, &outst);
	if(res < 0) return 1;
	
	for(i=0; i<task->dependencies->length; i++){
		fname = vector_get(task->dependencies, i);
		subtask = map_get(sqfile->tasks, fname);
		/* if there is a subtask that needs updating, this task needs updating */
		if(subtask && needs_update(subtask))
			return 1;
		res = stat(fname, &depst);
		if(res < 0) return 1;
		/* if the output file is older than the input, the task needs updating */
		if(outst.st_mtime < depst.st_mtime) return 1;
	}
	
	return 0;
}

void * run_commands(vector * commands){
	int i;
	char * command;

	for(i=0; i<commands->length; i++){
		command = vector_get(commands, i);
		fprintf(stderr, "%s\n", command);
		if(system(command)!=0) return NULL;
	}
	
	return (void*)THREAD_EXIT_OK;
}

void* perform_task(void * data){
	struct task * task = (struct task *)data;
	char * subtaskname;
	char * command;
	pthread_t tid;
	vector * threads = create_vector();
	struct task * subtask;
	int i, res, fd;
	void * retcode = (void*)THREAD_EXIT_OK;
	void * subretcode = (void*)THREAD_EXIT_OK;
	char lockfname[strlen(task->output) + 7];
	
	/* start threads for each dependency that has its own tasks and needs
		updating, putting their thread ids in threads */
	for(i=0; i<task->dependencies->length; i++){
		subtaskname = (char*)vector_get(task->dependencies, i);
		subtask = (struct task *)map_get(sqfile->tasks, subtaskname);
		if(subtask!=NULL && needs_update(subtask)){
			tid = start_task_thread(subtask);
			vector_add(threads, &tid, sizeof(tid));
		}
	}
	
	/* run and join the subtask threads */
	for(i=0; i<threads->length; i++){
		tid = *(pthread_t*)vector_get(threads, i);
		res = pthread_join(tid, &subretcode);
		if(subretcode == NULL || res!=0)
			retcode == NULL;
	}
	
	/* if none of the subtasks failed, run this task */
	if(retcode != NULL){
		/* put a lock on .output.lock */
		sprintf(lockfname, ".%s.lock", task->output);
		fd = creat(lockfname, 0644);
		if(fd >= 0){
			/* if we could not lock the file, wait until the file descriptor is
			unlocked (i.e. the locking thread has finished running) */
			if(flock(fd, LOCK_EX|LOCK_NB) < 0){
				flock(fd, LOCK_EX);
			} 
			/* otherwise, run the commands */
			else{
				retcode = run_commands(task->commands);
			}
		}
		/* unlock and remove the lockfile */
		flock(fd, LOCK_UN);
		remove(lockfname);
	}

	destroy_vector(threads);
	
	return retcode;
}

int main(int argc, char *argv[]){
	char * taskname;
	struct task * task;
	int line;
	void * res;
	FILE * f;

	f = fopen("Squadfile", "r");
	if(f==NULL) f = fopen("Makefile", "r");

	if(f==NULL){
		fprintf(stderr, "No Squadfile found\n");
		exit(EXIT_FAILURE);
	}
	
	sqfile = squad_file_new();
	line = parse_squad_file(f, sqfile);
	
	if(line > 0){
		fprintf(stderr, "Squadfile syntax error on line %d\n", line);
		exit(EXIT_FAILURE);
	}
	
	if(argc > 1) taskname = argv[1];
	else taskname = (char*)vector_get(sqfile->tasks->keys, 0);
	
	task = map_get(sqfile->tasks, taskname);
	
	if(task == NULL){
		fprintf(stderr, "Error: task %s not found\n", taskname);
		return EXIT_FAILURE;
	}
	
	if(needs_update(task))
		res = perform_task(task);
	else{
		fprintf(stderr, "%s already up to date\n", taskname);
		exit(EXIT_SUCCESS);
	}
	
	if(res == NULL)
		return EXIT_FAILURE;
		
	return 0;
}
