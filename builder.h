#ifndef __SQUAD_BUILDER_H__
#define __SQUAD_BUILDER_H__

#include "ds.h"

void* run_commands(vector * commands);
pthread_t start_task_thread(struct task * task);
int needs_update(struct task * task);
void* perform_task(void * data);

#endif /* __SQUAD_BUILDER_H__ */


