#include <string.h>
#include "parser.h"

void free_task(void * data){
	struct task * task = (struct task *) data;
	destroy_vector(task->commands);
	destroy_vector(task->dependencies);
	free(task);
}

int ignorable(char * line){
	int i;

	if(strlen(line)==0) return 1;
	if(line[0] == '#') return 1;
	
	for(i=0; line[i]; i++){
		if(line[i]!=' ' && line[i]!='\t')
			return 0;
	}
	
	return 1;
}

char * replace_vars(char * line, map * vars){
	char name[512];
	char * value;
	stringbuf * buf;
	char * str;
	int start=0;
	int len = strlen(line);
	int i, var = 0;
	
	buf = make_buffer(len);
	
	for(i=0; i<len; i++){
		if(line[i] == '$'){
			if(line[i+1] == '(' && i > start){
				buffer_nconcat(buf, line+start, i-start);
				start = i+2;
			}
		} else if(line[i] == ')'){
			if(i > start){
				strncpy(name, line+start, i-start);
				name[i-start] = 0;
				value = getenv(name);
				if(value == NULL || strlen(value)==0) value = map_get(vars, name);
				if(value != NULL && strlen(value)>0){
					buffer_concat(buf, value);
				}
				start = i+1;
			}
		}
	}
	
	if(i > start)
		buffer_nconcat(buf, line+start, i-start);
	
	str = buf->str;
	free(buf);
	
	return str;
}

squad_file * squad_file_new(){
	squad_file * sqfile = (squad_file *)malloc(sizeof(squad_file));
	sqfile->vars = create_map();
	sqfile->tasks = create_map();
	sqfile->tasks->destructor = free_task;
	return sqfile;
}

void squad_file_free(void * data){
	squad_file * sqfile = (squad_file *)sqfile;
	destroy_map(sqfile->vars);
	destroy_map(sqfile->tasks);
}

int parse_squad_file(FILE * f, squad_file * sqfile){
	char * str = saferead(f);
	vector * vec = str_split(str, "\n");
	int i;
	char * line;
	char * taskname = NULL;
	struct task task;

	for(i=0; i<vec->length; i++){
		line = vector_get(vec, i);
		
		if(ignorable(line)) continue;

		else if(strchr(line, '=')){
			char * name = strtok(line, "=");
			char * value = strtok(NULL, "=");
			map_put(sqfile->vars, name, value, strlen(value)+1);
		}
		else if(strchr(line, ':')){
			if(taskname){
				map_put(sqfile->tasks, taskname, &task, sizeof(task));
			}
			taskname = strtok(line, ":");
			task.output = (char*)malloc(strlen(taskname)+1);
			strcpy(task.output, taskname);
			char * depstr = strtok(NULL, ":");
			task.dependencies = str_split(depstr, " ");
			task.commands = create_vector();
		} 
		else if(line[0] == '\t'){
			char * command;
			command = replace_vars(line+1, sqfile->vars);
			vector_add(task.commands, command, strlen(command)+1);
			free(command);
		}
		else return i+1;
	}

	if(taskname){
		map_put(sqfile->tasks, taskname, &task, sizeof(task));
	}
	
	destroy_vector(vec);
	fclose(f);
	free(str);

	return 0;
}
